###############
Getting Started
###############
.. include:: laplace-intro.rst
.. include:: laplace-dirichlet.rst
.. include:: laplace-coefficients.rst
.. include:: laplace-la.rst
.. include:: laplace-adaptive.rst
.. include:: laplace-dg.rst

