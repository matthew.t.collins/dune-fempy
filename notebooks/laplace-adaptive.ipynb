{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Adaptive Finite Element [(Notebook)][1]\n",
    "\n",
    "[1]: _downloads/laplace-adaptive.ipynb\n",
    "We study the classic _re-entrant corner_ problem:\n",
    "\\begin{align*}\n",
    "  -\\Delta u &= f && \\text{in } \\Omega \\\\\n",
    "          u &= g && \\text{on } \\partial\\Omega\n",
    "\\end{align*}\n",
    "where the domain is given using polar coordinates\n",
    "\\begin{gather}\n",
    "  \\Omega = \\{ (r,\\varphi)\\colon r\\in(0,1), \\varphi\\in(0,\\Phi) \\}~.\n",
    "\\end{gather}\n",
    "For $g$ we take the trace of the function $u$, given by\n",
    "\\begin{gather}\n",
    "  u(r,\\varphi) = r^{\\frac{\\pi}{\\Phi}} \\sin\\big(\\frac{\\pi}{\\Phi} \\varphi \\big)\n",
    "\\end{gather}\n",
    "\n",
    "We first define the domain and set up the grid and space"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    %matplotlib inline # can also use notebook or nbagg\n",
    "except:\n",
    "    pass\n",
    "import math\n",
    "import numpy\n",
    "import matplotlib.pyplot as pyplot\n",
    "import dune.create as create\n",
    "from dune.fem.view import adaptiveLeafGridView\n",
    "from dune.fem.plotting import plotPointData as plot\n",
    "import dune.grid as grid\n",
    "import dune.fem as fem\n",
    "\n",
    "# set the angle for the corner (0<angle<=360)\n",
    "cornerAngle = 320.\n",
    "\n",
    "# use a second order space\n",
    "order = 2\n",
    "\n",
    "# define the grid for this domain (vertices are the origin and 4 equally spaced points on the\n",
    "# unit sphere starting with (1,0) and ending at # (cos(cornerAngle), sin(cornerAngle))\n",
    "vertices = numpy.zeros((8,2))\n",
    "vertices[0] = [0,0]\n",
    "for i in range(0,7):\n",
    "    vertices[i+1] = [math.cos(cornerAngle/6*math.pi/180*i),\n",
    "                     math.sin(cornerAngle/6*math.pi/180*i)]\n",
    "triangles = numpy.array([[2,1,0], [0,3,2], [4,3,0], [0,5,4], [6,5,0], [0,7,6]])\n",
    "domain = {\"vertices\": vertices, \"simplices\": triangles}\n",
    "view = create.view(\"adaptive\", \"ALUConform\", domain, dimgrid=2)\n",
    "view.hierarchicalGrid.globalRefine(2)\n",
    "spc  = create.space( \"lagrange\", view, dimrange=1, order=order )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next define the model together with the exact solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from ufl import *\n",
    "from dune.ufl import DirichletBC\n",
    "u = TrialFunction(spc)\n",
    "v = TestFunction(spc)\n",
    "x = SpatialCoordinate(spc.cell())\n",
    "\n",
    "# exact solution for this angle\n",
    "Phi = cornerAngle / 180 * math.pi\n",
    "phi = atan_2(x[1], x[0]) + conditional(x[1] < 0, 2*math.pi, 0)\n",
    "exact = as_vector([inner(x,x)**(math.pi/2/Phi) * sin(math.pi/Phi * phi)])\n",
    "a = inner(grad(u), grad(v)) * dx\n",
    "\n",
    "# set up the scheme\n",
    "laplace = create.scheme(\"h1\", spc, [a==0, DirichletBC(spc,exact,1)])\n",
    "uh = spc.interpolate(lambda x: [0], name=\"solution\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Theory tells us that\n",
    "\\begin{align*}\n",
    "  \\int_\\Omega \\nabla(u-u_h) \\leq \\sum_K \\eta_K\n",
    "\\end{align*}\n",
    "where on each element $K$ of the grid the local estimator is given by\n",
    "\\begin{align*}\n",
    "  \\eta_K = h_K^2 \\int_K |\\triangle u_h|^2 +\n",
    "    \\frac{1}{2}\\sum_{S\\subset \\partial K} h_S \\int_S [\\nabla u_h]^2\n",
    "\\end{align*}\n",
    "Here $[\\cdot]$ is the jump in normal direction over the edges of the grid.\n",
    "\n",
    "We compute the elementwise indicator by defining a bilinear form\n",
    "\\begin{align*}\n",
    "  \\eta(u,v) = h_K^2 \\int_K |\\triangle u_h|^2 v +\n",
    "    \\sum_{S\\subset \\partial K} h_S \\int_S [\\nabla u_h]^2 \\{v\\}\n",
    "\\end{align*}\n",
    "where $\\{\\cdot\\}$ is the average over the cell edges. This bilinear form can be easily written in UFL and by using it to define a discrete operator $L$ from the second order Lagrange space into a space containing piecewise constant functions \n",
    "we have $L[u_h]|_{K} = \\eta_K$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# energy error\n",
    "h1error = inner(grad(uh - exact), grad(uh - exact))\n",
    "\n",
    "# residual estimator\n",
    "fvspc = create.space(\"finitevolume\", view, dimrange=1, storage=\"istl\")\n",
    "estimate = fvspc.interpolate([0], name=\"estimate\")\n",
    "\n",
    "hT = MaxCellEdgeLength(spc.cell())\n",
    "he = MaxFacetEdgeLength(spc.cell())('+')\n",
    "n = FacetNormal(spc.cell())\n",
    "estimator_ufl = hT**2 * (div(grad(u[0])))**2 * v[0] * dx \\\n",
    "                + he * inner(jump(grad(u[0])), n('+'))**2 * avg(v[0]) * dS\n",
    "estimator_model = create.model(\"integrands\", view, estimator_ufl == 0)\n",
    "estimator = create.operator(\"galerkin\", estimator_model, spc, fvspc)\n",
    "\n",
    "# marking strategy (equidistribution)\n",
    "tolerance = 0.1\n",
    "gridSize = view.size(0)\n",
    "def mark(element):\n",
    "    estLocal = estimate(element, element.geometry.referenceElement.center)\n",
    "    return grid.Marker.refine if estLocal[0] > tolerance / gridSize else grid.Marker.keep"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# adaptive loop (solve, mark, estimate)\n",
    "fig = pyplot.figure(figsize=(10,10))\n",
    "count = 0\n",
    "while count < 20:\n",
    "    laplace.solve(target=uh)\n",
    "    if count%3 == 0:\n",
    "        pyplot.show()\n",
    "        pyplot.close('all')\n",
    "        fig = pyplot.figure(figsize=(10,10))\n",
    "    plot(uh,figure=(fig,131+count%3), colorbar=False)\n",
    "    # compute the actual error and the estimator\n",
    "    error = math.sqrt(fem.function.integrate(view, h1error, 5)[0])\n",
    "    estimator(uh, estimate)\n",
    "    eta = sum(estimate.dofVector)\n",
    "    print(count, \": size=\", gridSize, \"estimate=\", eta, \"error=\", error)\n",
    "    if eta < tolerance:\n",
    "        break\n",
    "    if tolerance == 0.:\n",
    "        view.hierarchicalGrid.globalRefine(2)\n",
    "        uh.interpolate([0])  # initial guess needed\n",
    "    else:\n",
    "        marked = view.hierarchicalGrid.mark(mark)\n",
    "        fem.adapt(view.hierarchicalGrid, [uh])\n",
    "        fem.loadBalance(view.hierarchicalGrid, [uh])\n",
    "    gridSize = view.size(0)\n",
    "    laplace.solve( target=uh )\n",
    "    count += 1\n",
    "pyplot.show()\n",
    "pyplot.close('all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look at the center of the domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig = pyplot.figure(figsize=(15,15))\n",
    "plot(uh, figure=(fig,131+0), xlim=(-0.5,0.5), ylim=(-0.5,0.5),colorbar={\"shrink\":0.3})\n",
    "plot(uh, figure=(fig,131+1), xlim=(-0.25,0.25), ylim=(-0.25,0.25),colorbar={\"shrink\":0.3})\n",
    "plot(uh, figure=(fig,131+2), xlim=(-0.125,0.125), ylim=(-0.125,0.125),colorbar={\"shrink\":0.3})\n",
    "pyplot.show()\n",
    "pyplot.close('all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let us have a look at the grid levels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from dune.fem.function import levelFunction\n",
    "plot(levelFunction(view), xlim=(-0.2,1), ylim=(-0.2,1))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
