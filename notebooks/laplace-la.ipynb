{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Different storage backends - using numpy/scipy and petsc4py [(Notebook)][1]\n",
    "\n",
    "[1]: _downloads/laplace-la.ipynb\n",
    "\n",
    "For the first example we used solvers available in dune-fem - simple Krylov solvers with only diagonal preconditioning. Changing the `storage` argument in the construction of the space makes it possible to use more sophisticated solvers (either better preconditioners or direct solvers). For example\n",
    "~~~\n",
    "spc = create.space(\"lagrange\", grid, dimrange=1, order=1, storage=\"istl\")\n",
    "~~~\n",
    "in the above code will switch to the solvers from `dune-istl`, other options are for example `eigen` or `petsc`.\n",
    "\n",
    "Using the internal `fem` storage structure or the `eigen` matrix/vector strorage\n",
    "it is also possible to directly treate them as`numpy` vectors and an assembled system matrix can be stored in a `sympy` sparse matrix.\n",
    "\n",
    "__Note__: to use `eigen` matrices the `Eigen` package must be available and `dune-py` must have been configured with `Eigen`.\n",
    "\n",
    "Since we will be implementing a Newton solver first, let's study a truelly non linear problem - a version of the p-Laplace problem:\n",
    "\\begin{gather}\n",
    "  - \\frac{d}{2}\\nabla\\cdot |\\nabla u|^{p-2}\\nabla u + u = f\n",
    "\\end{gather}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    %matplotlib inline # can also use notebook or nbagg\n",
    "except:\n",
    "    pass\n",
    "from dune.generator import builder\n",
    "import math\n",
    "import numpy as np\n",
    "import scipy.sparse.linalg\n",
    "import scipy.optimize     \n",
    "import dune.grid\n",
    "import dune.fem\n",
    "from dune.fem.plotting import plotPointData as plot\n",
    "import dune.create as create\n",
    "\n",
    "from dune.ufl import Space\n",
    "from ufl import TestFunction, TrialFunction, SpatialCoordinate, ds, dx, inner, grad\n",
    "\n",
    "grid = create.grid(\"ALUConform\", dune.grid.cartesianDomain([0, 0], [1, 1], [8, 8]), dimgrid=2)\n",
    "\n",
    "spc = create.space(\"lagrange\", grid, dimrange=1, order=1, storage='fem')\n",
    "\n",
    "d = 0.001\n",
    "p = 1.7\n",
    "\n",
    "u = TrialFunction(spc)\n",
    "v = TestFunction(spc)\n",
    "x = SpatialCoordinate(spc.cell())\n",
    "\n",
    "rhs = (x[0] + x[1]) * v[0]\n",
    "a = (pow(d + inner(grad(u), grad(u)), (p-2)/2)*inner(grad(u), grad(v)) + inner(u, v)) * dx + 10*inner(u, v) * ds\n",
    "b = rhs * dx + 10*rhs * ds\n",
    "scheme = create.scheme(\"h1\", spc, a==b,\\\n",
    "       parameters=\\\n",
    "       {\"fem.solver.newton.linabstol\": 1e-10,\n",
    "        \"fem.solver.newton.linreduction\": 1e-10,\n",
    "        \"fem.solver.newton.verbose\": 1,\n",
    "        \"fem.solver.newton.linear.verbose\": 0})\n",
    "# create a discrete solution over this space - will be initialized with zero by default\n",
    "\n",
    "uh = create.function(\"discrete\", spc, name=\"solution\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following we implement a simple Newton solver: given an initial guess $u^0$ (here taken to be zero) solve for $n\\geq 0$:\n",
    "\\begin{align*}\n",
    "   u^{n+1} = u^n - DS(u^n)(S(u^n)-g)\n",
    "\\end{align*}\n",
    "Where $g$ is a discrete function containing the boundary values in the Dirichlet nodes and zero otherwise.\n",
    "\n",
    "Let's first use the solve method on the scheme directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "uh,info = scheme.solve(target = uh)\n",
    "print(info)\n",
    "plot(uh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of `scheme.solve` we now use the call operator on the `scheme` (to compute $S(u^n$) as  well as `scheme.assemble` to get a copy of the system matrix in form of a scipy sparse row matrix. Note that this method is only available if the `storage` in the space is set `eigen`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Let's first clear the solution again\n",
    "uh.clear()\n",
    "# Need to auxiliary function\n",
    "res = uh.copy() \n",
    "\n",
    "# Note: the following does not produce a copy of the dof\n",
    "# vectors, but keep in mind that\n",
    "# after grid adaptation the resulting numpy array\n",
    "# will be invalid since the shared dof vector will have moved\n",
    "# during its resizing - use copy=True to avoid this problem at\n",
    "# the cost of a copy\n",
    "sol_coeff = uh.as_numpy\n",
    "res_coeff = res.as_numpy\n",
    "n = 0\n",
    "\n",
    "while True:\n",
    "    scheme(uh, res)\n",
    "    absF = math.sqrt( np.dot(res_coeff,res_coeff) )\n",
    "    print(\"iterations (\"+str(n)+\")\",absF)\n",
    "    if absF < 1e-10:\n",
    "        break\n",
    "    matrix = scheme.assemble(uh)\n",
    "    sol_coeff -= scipy.sparse.linalg.spsolve(matrix, res_coeff)\n",
    "    n += 1\n",
    "\n",
    "plot(uh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We cam redo the above computation but now use the Newton solver available in sympy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# let's first set the solution back to zero - since it already contains the right values\n",
    "uh.clear()\n",
    "def f(x_coeff):                     \n",
    "    x = spc.numpyFunction(x_coeff, \"tmp\")\n",
    "    scheme(x,res)         \n",
    "    return res_coeff\n",
    "# class for the derivative DS of S\n",
    "class Df(scipy.sparse.linalg.LinearOperator):\n",
    "    def __init__(self,x_coeff):\n",
    "        self.shape = (sol_coeff.shape[0],sol_coeff.shape[0])\n",
    "        self.dtype = sol_coeff.dtype\n",
    "        # the following converts a given numpy array\n",
    "        # into a discrete function over the given space\n",
    "        x = spc.numpyFunction(x_coeff, \"tmp\")\n",
    "        # store the assembled matrix\n",
    "        self.jac = scheme.assemble(x)\n",
    "    # reassemble the matrix DF(u) gmiven a dof vector for u\n",
    "    def update(self,x_coeff,f): \n",
    "        x = spc.numpyFunction(x_coeff, \"tmp\")\n",
    "        # Note: the following does produce a copy of the matrix\n",
    "        # and each call here will reproduce the full matrix\n",
    "        # structure - no reuse possible in this version\n",
    "        self.jac = scheme.assemble(x)\n",
    "    # compute DS(u)^{-1}x for a given dof vector x\n",
    "    def _matvec(self,x_coeff):\n",
    "        return scipy.sparse.linalg.spsolve(self.jac, x_coeff)\n",
    "\n",
    "# call the newton krylov solver from scipy\n",
    "sol_coeff[:] = scipy.optimize.newton_krylov(f, sol_coeff,\n",
    "            verbose=1, f_tol=1e-8,\n",
    "            inner_M=Df(sol_coeff))\n",
    "\n",
    "plot(uh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use the package `petsc4py` to solve the problem.\n",
    "\n",
    "__Note__: make sure that `dune` has been configured using the same version of `petsc` used for `petsc4py`\n",
    "\n",
    "The first step is to change the storage in the space. Since also requires setting up the scheme and siscrete functions again to use the new storage structure.\n",
    "\n",
    "We can directly use the `petsc` solvers by invoking `solve` on the scheme as before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "try:    \n",
    "    import petsc4py, sys\n",
    "    from petsc4py import PETSc\n",
    "    petsc4py.init(sys.argv)\n",
    "    spc = create.space(\"lagrange\", grid, dimrange=1, order=1, storage='petsc')\n",
    "    scheme = create.scheme(\"h1\", spc, a==b,\n",
    "                            parameters={\"petsc.preconditioning.method\":\"sor\"})\n",
    "    # first we will use the petsc solver available in the `dune-fem` package (using the sor preconditioner)\n",
    "    uh, info = scheme.solve()\n",
    "    print(info)\n",
    "    plot(uh)\n",
    "except ImportError:\n",
    "    print(\"petsc4py could not be imported\")\n",
    "    petsc4py = False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we will implement the Newton loop in Python using `petsc4py` to solve the linear systems\n",
    "Need to auxiliary function and set `uh` back to zero.\n",
    "We can access the `petsc` vectors by calling `as_petsc` on the discrete function. Note that this property will only be available if the discrete function is an element of a space with storage `petsc`.\n",
    "The method `assemble` on the scheme now returns the sparse `petsc` matrix and so we can direclty use the `ksp` class from `petsc4py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "if petsc4py:\n",
    "    uh.clear()\n",
    "    res = uh.copy() \n",
    "\n",
    "    sol_coeff = uh.as_petsc\n",
    "    res_coeff = res.as_petsc\n",
    "\n",
    "    ksp = PETSc.KSP()\n",
    "    ksp.create(PETSc.COMM_WORLD)\n",
    "    # use conjugate gradients method\n",
    "    ksp.setType(\"cg\")\n",
    "    # and incomplete Cholesky\n",
    "    ksp.getPC().setType(\"icc\")\n",
    "\n",
    "    n = 0\n",
    "    while True:\n",
    "        scheme(uh, res)\n",
    "        absF = math.sqrt( res_coeff.dot(res_coeff) )\n",
    "        print(\"iterations (\"+str(n)+\")\",absF)\n",
    "        if absF < 1e-10:\n",
    "            break\n",
    "        matrix = scheme.assemble(uh)\n",
    "        ksp.setOperators(matrix)\n",
    "        ksp.setFromOptions()\n",
    "        ksp.solve(res_coeff, res_coeff)\n",
    "        sol_coeff -= res_coeff\n",
    "        n += 1\n",
    "    plot(uh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we weill use `petsc`'s non-linear solvers (the `snes` classes) directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "if petsc4py:\n",
    "    uh.clear()\n",
    "    def f(snes, X, F):\n",
    "        inDF = spc.petscFunction(X)\n",
    "        outDF = spc.petscFunction(F)\n",
    "        scheme(inDF,outDF)\n",
    "    def Df(snes, x, m, b):\n",
    "        inDF = spc.petscFunction(x)\n",
    "        matrix = scheme.assemble(inDF)\n",
    "        m.createAIJ(matrix.size, csr=matrix.getValuesCSR())\n",
    "        b.createAIJ(matrix.size, csr=matrix.getValuesCSR())\n",
    "        return PETSc. Mat. Structure.SAME_NONZERO_PATTERN\n",
    "\n",
    "    snes = PETSc.SNES().create()\n",
    "    snes.setMonitor(lambda snes,i,r:print(i,r,flush=True))\n",
    "    snes.setFunction(f, res_coeff)\n",
    "    # snes.setUseMF(True)\n",
    "    snes.setJacobian(Df,matrix,matrix)\n",
    "    snes.getKSP().setType(\"cg\")\n",
    "    snes.setFromOptions()\n",
    "    snes.solve(None, sol_coeff)\n",
    "    plot(uh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Note__: \n",
    "The method `as_numpy, as_petsc` returning the`dof` vector either as a `numpy` or a `petsc` do not lead to a copy of the data and the same is true fr the `numpyFunction` and the `petscFunction` methods on the space. In the `numpy` case we can use `Python`'s buffer protocol to use the same underlying storage. In the case of `petsc` the underlying `Vec` can be shared. In the case of matrices the situation is not yet as clear: `scheme.assemble` returns a copy of the data in the `scipy` case while the `Mat` structure is shared between `c++` and  `Python` in the `petsc` case. But at the time of writting it is not possible to pass in the `Mat` structure to the `scheme.assemble` method from the outside. That is why it is necessary to copy the data when using the `snes` non linear solver as seen above."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
