{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Spiral Wave [(Notebook)][1]\n",
    "\n",
    "[1]: _downloads/spiral.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "This demonstrates the simulation of spiral waves in an excitable media. It consists of system of reaction diffusion equations with two components. Both the model parameters and the approach for discretizing the system are taken from http://www.scholarpedia.org/article/Barkley_model.\n",
    "\n",
    "We use the _Barkley model_ in its simplest form:\n",
    "\\begin{align*}\n",
    "  \\frac{\\partial u}{\\partial_t} \n",
    "       &= \\frac{1}{\\varepsilon}f(u,v) + \\Delta u \\\\\n",
    "  \\frac{\\partial v}{\\partial_t} &= h(u,v)\n",
    "\\end{align*}\n",
    "where\n",
    "\\begin{gather}\n",
    "  f(u,v(=u\\Big(1-u\\Big)\\Big(u-\\frac{v+b}{a}\\Big)\n",
    "\\end{gather}\n",
    "The function $h$ can take different forms, e.g., in its simplest form\n",
    "\\begin{gather}\n",
    "  h(u,v) = u - v~.\n",
    "\\end{gather}\n",
    "Finally, $\\varepsilon,a,b$ for more details on how to chose these parameters check the web page provided above.\n",
    "\n",
    "We employ a carefully constructed linear time stepping scheme for this model: let $u^n,v^n$ be given functions approximating the solution at a time $t^n$. To compute approximations $u^{m+1},v^{m+1}$ at a later time\n",
    "$t^{n+1}=t^n+\\tau$ we first split up the non linear function $f$ as follows:\n",
    "\\begin{align*}\n",
    "  f(u,v) = f_I(u,u,v) + f_E(u,v)\n",
    "\\end{align*}\n",
    "where using $u^*(V):=\\frac{V+b}{a}$:\n",
    "\\begin{align*}\n",
    "  f_I(u,U,V) &= \\begin{cases}\n",
    "    u\\;(1-U)\\;(\\;U-U^*(V)\\;) & U < U^*(V) \\\\\n",
    "    -u\\;U\\;(\\;U-U^*(V)\\;)    & U \\geq U^*(V) \n",
    "  \\end{cases} \\\\\n",
    "\\text{and} \\\\\n",
    "    f_E(U,V) &= \\begin{cases}\n",
    "    0 & U < U^*(V) \\\\\n",
    "    U\\;(\\;U-U^*(V)\\;)    & U \\geq U^*(V) \n",
    "  \\end{cases} \\\\\n",
    "\\end{align*}\n",
    "Thus $f_I(u,U,V) = -m(U,V)u$ with\n",
    "\\begin{align*}\n",
    "  m(U,V) &= \\begin{cases}\n",
    "    (U-1)\\;(\\;U-U^*(V)\\;) & U < U^*(V) \\\\\n",
    "    U\\;(\\;U-U^*(V)\\;)    & U \\geq U^*(V) \n",
    "  \\end{cases}\n",
    "\\end{align*}\n",
    "Note that $u,v$ are assumed to take values only between zero and one so that therefore $m(u^n,v^n) > 0$. Therefore, the following time discrete version of the Barkley model has a linear, positive definite elliptic operator on its left hand side:\n",
    "\\begin{align*}\n",
    "  -\\tau\\Delta u^{n+1} + \n",
    "   (1+\\frac{\\tau}{\\varepsilon} m(u^n,v^n))\\; u^{n+1} \n",
    "       &= u^n + \\frac{\\tau}{\\varepsilon} f_E(u^n,v^n) \\\\\n",
    "  v^{n+1} &= v^n + \\tau h(u^n,v^n)\n",
    "\\end{align*}\n",
    "Which can now be solved using a finite element discretization for $u^n,v^n$.\n",
    "\n",
    "Note that by taking the slow reaction $h(u,v)$ explicitly, the equation for $v^{n+1}$ is purely algebraic. We will therefore construct a scalar model for computing $u^{n+1}$ only and compute $v^{{n+1}}$ be using the interpolation method on the space applied to\n",
    "$v^n + \\tau h(u^n,v^n)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Let's get started by importing some standard python packages, ufl, and some part of the dune-fempy package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "try:\n",
    "    %matplotlib inline # can also use notebook or nbagg\n",
    "except:\n",
    "    pass\n",
    "import math\n",
    "from functools import reduce\n",
    "\n",
    "import ufl\n",
    "\n",
    "import dune.ufl\n",
    "import dune.grid\n",
    "import dune.models.elliptic\n",
    "import dune.fem\n",
    "import dune.create as create"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "In our attempt we will discretize the model as a 2x2 system. Here are some possible model parameters and initial conditions (we even have two sets of model parameters to choose from):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "dimRange   = 1\n",
    "dt         = 0.25\n",
    "if 1:\n",
    "    spiral_a   = 0.75\n",
    "    spiral_b   = 0.02\n",
    "    spiral_eps = 0.02\n",
    "    spiral_D   = 1./100\n",
    "    def spiral_h(u,v): return u - v\n",
    "else:\n",
    "    spiral_a   = 0.75\n",
    "    spiral_b   = 0.0006\n",
    "    spiral_eps = 0.08\n",
    "    def spiral_h(u,v): return u**3 - v\n",
    "\n",
    "initial_u = lambda x: [1   if x[1]>1.25 else 0]\n",
    "initial_v = lambda x: [0.5 if x[0]<1.25 else 0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Now we set up the reference domain, the Lagrange finite element space (second order), and discrete functions for $(u^n,v^n($, $(u^{n+1},v^{n+1})$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# domain = dune.grid.cartesianDomain([0,0],[3.5,3.5],[40,40])\n",
    "domain = dune.grid.cartesianDomain([0,0],[2.5,2.5],[30,30])\n",
    "grid = create.grid(\"ALUCube\", domain, dimgrid=2)\n",
    "spc  = create.space( \"lagrange\", grid, dimrange=dimRange, order=1 )\n",
    "\n",
    "uh   = spc.interpolate( initial_u, name=\"u\" )\n",
    "uh_n = uh.copy()\n",
    "vh   = spc.interpolate( initial_v, name=\"v\" )\n",
    "vh_n = vh.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We define the model in two steps:\n",
    "- first we define the standard parts, not involving $f_E,f_I$:\n",
    "- then we add the missing parts with the required _if_ statement directly using C++ code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "uflSpace = dune.ufl.Space((grid.dimGrid, grid.dimWorld), dimRange)\n",
    "u   = ufl.TrialFunction(uflSpace)\n",
    "phi = ufl.TestFunction(uflSpace)\n",
    "un  = ufl.Coefficient(uflSpace)\n",
    "vn  = ufl.Coefficient(uflSpace)\n",
    "\n",
    "# right hand sie (time derivative part + explicit forcing in v)\n",
    "a_ex = ufl.inner(un, phi) * ufl.dx \n",
    "# left hand side (heat equation in first variable + backward Euler in time)\n",
    "a_im = (dt * spiral_D * ufl.inner(ufl.grad(u), ufl.grad(phi)) + \n",
    "        ufl.inner(u,phi)) * ufl.dx\n",
    "\n",
    "ustar = (vn[0]+spiral_b)/spiral_a\n",
    "a_ex += ufl.conditional(un[0]<ustar, dt/spiral_eps* u[0]*(1-un[0])*(un[0]-ustar),\n",
    "                                     dt/spiral_eps*un[0]*(1-u[0]) *(un[0]-ustar) ) * phi[0] * ufl.dx\n",
    "\n",
    "equation = a_im == a_ex"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "rhs_gf = create.function(\"ufl\", grid, \"rhs\", order=2, \n",
    "                         ufl=ufl.as_vector( [vn[0] + dt*spiral_h(un[0], vn[0]) ]),\n",
    "                         coefficients={un: uh_n, vn: vh_n} )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "The model is now completely implemented and can be created, together with the corresponding scheme:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "model = create.model(\"elliptic\", grid,  equation,  coefficients={un: uh_n, vn: vh_n} )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "solverParameters = {\n",
    "        \"fem.solver.newton.tolerance\": 1e-3,\n",
    "        \"fem.solver.newton.linabstol\": 1e-5,\n",
    "        \"fem.solver.newton.linreduction\": 1e-5,\n",
    "        \"fem.solver.newton.verbose\": 0,\n",
    "        \"fem.solver.newton.linear.verbose\": 0\n",
    "    }\n",
    "scheme = create.scheme(\"h1\", spc, model, (\"pardg\",\"cg\"),parameters=solverParameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "To show the solution we make use of the _animate_ module of _matplotlib_:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from numpy import linspace\n",
    "from matplotlib import animation, rc\n",
    "rc('animation', html='html5')\n",
    "fig, ax = plt.subplots()\n",
    "ax.set_xlim(( 0, 2.5))\n",
    "ax.set_ylim(( 0, 2.5))\n",
    "triangulation = grid.triangulation(1)\n",
    "levels = linspace(-0.1, 1.1, 256)\n",
    "ax.set_aspect('equal')\n",
    "t        = 0.\n",
    "stepsize = 0.5\n",
    "nextstep = 0.\n",
    "iterations = 0\n",
    "\n",
    "def animate(count):  \n",
    "    global t, stepsize, nextstep, iterations, dt\n",
    "    # print('frame',count,t)\n",
    "    while t < nextstep: \n",
    "        uh_n.assign(uh)\n",
    "        vh_n.assign(vh)\n",
    "        _,info = scheme.solve(target=uh)\n",
    "        vh.interpolate( rhs_gf )\n",
    "        # print(\"Computing solution a t = \" + str(t + dt), \"iterations: \" + info[\"linear_iterations\"] )\n",
    "        iterations += int( info[\"linear_iterations\"] )\n",
    "        t     += dt\n",
    "    data = uh.pointData(1)\n",
    "    plt.tricontourf(triangulation, data[:,0], cmap=plt.cm.rainbow, levels=levels)\n",
    "    grid.writeVTK(\"spiral\", pointdata=[uh], number=count)\n",
    "    nextstep += stepsize\n",
    "    \n",
    "animation.FuncAnimation(fig, animate, frames=25, interval=100, blit=False)"
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 1
}
